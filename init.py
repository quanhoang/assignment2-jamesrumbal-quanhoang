import os, os.path,json
import shutil


def init(archiveDir,objectsDir,indexPath):
    if os.path.exists(objectsDir) and os.path.exists(indexPath):
        print("The archive has already been initialized.")
        return
    else:
        try:
            shutil.rmtree(archiveDir)
        except OSError as e:
            pass
        os.makedirs(objectsDir)
        with open(indexPath, 'w') as file:
            json.dump({}, file)
    print("Created myArchive directory")
    return
