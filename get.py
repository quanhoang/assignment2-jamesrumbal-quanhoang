import hashFinder, os, json, shutil
from hashFinder import createFileSignature


def get(searchWord,objectsDir,indexPath):
    matches = [];
    index = {}

    try:
        with open(indexPath) as file:
            index = json.load(file);
    except (OSError, IOError):
        print ("Error loading index file. Index could be missing or not created yet");
        return

    for key in index.keys():
        if searchWord in os.path.basename(key):
            matches.append(key);

    if (len(matches) == 0):
        print ("Could not find a file called " + searchWord);

    elif (len(matches) == 1):
        file_path = os.path.join(os.getcwd(),os.path.basename(matches[0]));
        override = True;

        if os.path.exists(file_path):
            choice = raw_input("This file currently exists in the current directory. Do you wish to override? [y/n]: ");
            if (choice.lower() != "y"):
                override = False;

        if (override):
            try:
                shutil.copy(os.path.join(objectsDir,index[matches[0]]), file_path);
                print("Restored the matched file in current working directory");
            except (shutil.Error, IOError):
                print ("Could not restore the file in the current directory. You may need administrator access.");
                return;

    else:
        print ("Please choose which file you wish to choose ('0' chooses none): ");
        if (len(matches) > 50):
            for n in range(0,51):
                print (str(n+1) + ": " + matches[n]);
        else:
            for n in range(0,len(matches)):
                print (str(n+1) + ": " + matches[n]);

        try:
            user_input = (int(raw_input("Choice [#]: ")));
            
            if (user_input == 0):
                return;
                
            if (user_input <= 50 and user_input > 0 and user_input <= len(matches)):
                override = True;
    
                if os.path.exists(os.path.join(os.getcwd(),os.path.basename(matches[user_input-1]))):
                    choice = raw_input("This file currently exists in the current directory. Do you wish to override? [y/n]: ");
                    if (choice.lower() != "y"):
                        override = False;
    
                if (override):
                    try:
                        shutil.copy(os.path.join(objectsDir,index[matches[user_input-1]]), os.path.join(os.getcwd(),os.path.basename(matches[user_input-1])));
                        print ("File restored.");
                    except (shutil.Error, IOError):
                        if (not os.path.exists(os.path.join(objectsDir,index[matches[user_input-1]]))):
                            print ("The requested file does not exist in objects");
                        else:
                            print ("Could not restore the file in the current directoryYou may need administrator access.");
                            return;
        except (IOError, EOFError, ValueError):
            print ("\nInput was invalid.")



