#!/usr/bin/env python

import store,restore,list, sys, os
from store import store
from list import list
from init import init
from restore import restore
from get import get
from test import test

archiveDir = os.path.expanduser("~/Desktop/myArchive")
objectDir = os.path.join(archiveDir, "objects")
indexPath = os.path.join(archiveDir, "index")

def main():
    if len(sys.argv) == 2:
        if str.lower(sys.argv[1]) == "init":
                init(archiveDir,objectDir, indexPath)
        elif str.lower(sys.argv[1]) == "help" :
                    print("To run the program. Type \"myBackup.py\" in the command line followed by\n")
                    print("\tinit -  initialise the archive directory")
                    print("\tstore <full path dir> -  add the specified directory to the archive")
                    print("\tlist <pattern> - List the archive contents that match the given pattern, if there is no matched result, list all file in the archive.")
                    print("\ttest - check that the objects folder isn't damaged")
                    print("\tget - recover a single file")
                    print("\trestore <full path dir> - recover a copy of everything the archive to the specified directory")
                    print("\trestore - recover a copy of everything the archive to the current working directory")
                    print("\thelp - instruction")
        elif (str.lower(sys.argv[1]) in ["restore", "test", "list"]):
            if os.path.exists(objectDir) and os.path.isdir(objectDir) and os.path.exists(indexPath) and os.path.isfile(indexPath):
                if str.lower(sys.argv[1]) == "restore":
                        destDir = os.getcwd()
                        restore(destDir, objectDir, indexPath)

                if str.lower(sys.argv[1]) == "test":
                    test(objectDir,indexPath)
                    
                if str.lower(sys.argv[1]) == "list":
                    list(indexPath)
            else:
                print("Error: Loading required directories and files.")
                print("Run init to create new archive directory or check your archive directory")
        else:
                print("Invalid syntax. See 'myBackup.py help' for more information")
                return
    elif len(sys.argv) == 3:
        if (str.lower(sys.argv[1]) in ["store", "restore", "get", "list"]):
            if os.path.exists(objectDir) and os.path.isdir(objectDir) and os.path.exists(indexPath) and os.path.isfile(indexPath):
                if str.lower(sys.argv[1]) == "store":
                        store(sys.argv[2],archiveDir,objectDir, indexPath)
                if str.lower(sys.argv[1]) == "list":
                        list(indexPath, pattern=sys.argv[2])
                elif str.lower(sys.argv[1]) == "restore":
                        destDir = sys.argv[2]
                        restore(destDir, objectDir, indexPath)

                elif str.lower(sys.argv[1]) == "get":
                        get(sys.argv[2], objectDir, indexPath)
            else:
                print("Error: Loading required directories and files.")
                print("Run init to create new archive directory or check your archive directory")
        else:
                print("Invalid syntax. See 'myBackup.py help' for more information.")

    else:
        print("Invalid syntax. See 'myBackup.py help' for more information.")



if __name__ == '__main__':
    main()
