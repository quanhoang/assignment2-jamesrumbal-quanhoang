"""
Create a log file handler which logs message to both the screen and the keyboard.
The logfile can be either a rotating logfile or continuous.

"""
import os
import logging
import logging.handlers


def log():
    program_name = "myBackup"
    log_filename = os.path.expanduser("~/Desktop/myArchive/myBackup.log")

    # console_log_level = logging.ERROR  # Only show errors to the console
    file_log_level   = logging.INFO   # but log info messages to the logfile

    logger = logging.getLogger(program_name)
    logger.setLevel(logging.DEBUG)

    #====================================================================================
    # FILE-BASED LOG

    # Create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    #formatter = logging.Formatter('%(levelname)s - %(message)s')

    # LOGFILE HANDLER - SELECT ONE OF THE FOLLOWING TWO LINES

    fh = logging.FileHandler(log_filename)                          # Continuous Single Log
    # fh = logging.handlers.RotatingFileHandler(Log_filename, backupCount=5) # Rotating log

    fh.setLevel(file_log_level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    return logger