import json, os

def list(indexPath, pattern=None):
    try:
        with open(indexPath) as file:
            index = json.load(file)
    except (OSError, IOError):
        print ("Error loading index file. Index could be missing or not created yet");
        return
    found = 0
    if pattern is not None:
        for key in index.keys():
            if pattern in key:
                print(key)
                found = 1
        if not found:
            print ('No file matches pattern\n')
    else:
        for key in index.keys():
            print (key)