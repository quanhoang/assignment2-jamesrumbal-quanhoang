import os, json, shutil


def restore(destDir,objectDir,indexPath):
    index = {}
    new_index = {}
    stored_hash = []
    if not os.path.exists(destDir):
        print ("Destination directory is not exist")
        return
    if not os.path.isdir(destDir):
        print ("Destination directory is not a directory")
        return
    # Load the index into dictionary
    if os.path.exists(indexPath):
       with open(indexPath) as file:
        index = json.load(file)
    # Rework the dictionary, convert relative paths into new full path
    for file in index.keys():
        new_path = os.path.join(destDir, file)
        new_index[new_path] = index[file]
    # Makes all the required directory and subdirectory in the restore destination folder
    for path in new_index.keys():
        try:
             os.makedirs(os.path.dirname(path))
        except os.error:
             pass
    # Restore all files from objects directory into destination directory
    error = 0;
    for path in new_index.keys():
        try:
            shutil.copy(os.path.join(objectDir,new_index[path]), path)
        except IOError:
            print('The objects ' + new_index[path] +' is missing or damage.')
            print('The file '+ path + ' cannot be recovered')
            error += 1;
            pass;
    if error:
        print(str(error) + ' files cannot be recovered')