# README #

This README would normally document whatever steps are necessary to get your application up and running.

## 159.251 Software Engineering Design & Construction
## Group Assignment 2
### Group members:

* James Rumbal   : ID - 12331075
* Minh Quan Hoang: ID - 13127565

### How to run myBackup program ###

1. Clone the program via ssh or https to your local machine from bitbucket with password: assignment2. Skip this step if you already got the program on your computer
2. Open command line(Windows) or terminal (Linux/Unix/Mac) and mount to the program directory
3. Type "python myBackup.py" followed by any of the below command to perform the desired tasks

	 - help - open instruction
     - init - initialize the archive directory
     - store <full dir> - add the specified directory to the archive
     - list <pattern> - List the archive contents that match the given pattern, if there is no matched result, list all file in the archive.
     - test - check that the objects folder isn't damaged
     - get <pattern>- recover a single file that matches the specified pattern
     - restore <full dir> - recover a copy of everything the archive to the specified directory
	 - restore - recover a copy of everything the archive to the current working directory

Note: Unix/Linux user might have to provide administrator access when using one of the above command. If there is "Some thing gone wrong". Please make sure you got administrator access.

### Contribution guidelines ###

* James Rumbal

	- Primarily responsible for myBackup, init, test and get module. Also significantly contribute to store, restore and list modules
	- Contribute total code quality control, testing plan, feature and functinality developement 
	- Contribute to design, improving code, code review, debugging, testing the program
	- Significant commits : 
	
		+ ef3b4a3
		+ 91a0202
		+ 7df891a
		+ 071542d
		+ a536b77
		+ 4fc17c9
		+ 5e752ab
		+ 46171d9
		+ 6e6557c
		+ 9f128c9
		+ 9277a36
		+ cc8b12b
		+ fbac670
		+ 72c3c77
		+ 6305d65
		+ b2cb610
		+ e6ffcfb
		+ 0dece34
		
		
* Minh Quan Hoang

	- Responsible for myBackup , init, store, list, restore, log module. Also partitially contribute to others modules.
	- Overall code quality control, project management 
	- Contribute to design, improving code, code review, debugging, testing the program
	- Significant commits : 
	
		+ 86fcecd
		+ 118d917
		+ 925dc16
		+ ee3e3d3
		+ 90929ba
		+ 954a965
		+ 38245c9
		+ c13242d
		+ d6ad5e4
		+ 4bfe49c
		+ 208960f
		+ 6f9246c
		+ fb4610e
		+ 4a23128
		+ 93da0aa
		+ 17e9863
