import os
import json
import shutil
from log import log
from hashFinder import createFileSignature


def store(userEnterDirectory, archiveDirectory, objDirectory, indexPath):
    new_files_counter = 0
    existed_files_counter = 0

    if not os.path.isdir(userEnterDirectory):
        print ("Provided argument is not a directory")
        return

    if not os.path.exists(userEnterDirectory):
        print ("Provided directory is not exist")
        return

    logger = log()
    logger.info("Store session started\n")

    try:
        with open(indexPath) as file:
            index = json.load(file)
    except IOError as e:
        print("Error loading index file: " + e.strerror)
        return

    for root, dirs, files in os.walk(userEnterDirectory):

        for name in files:
            file_path = os.path.join(root, name)
            file_signature = createFileSignature(file_path)
            relative_path = os.path.relpath(file_path,userEnterDirectory)
            new_file_path = os.path.join(objDirectory, file_signature[2])

            if relative_path not in index.keys():
                # Convert full path into relative path then store it in dictionary with corresponding hash value
                index[relative_path] = file_signature[2]
                new_files_counter += 1

                if not (os.path.exists(new_file_path) and os.path.isfile(new_file_path)):
                    shutil.copy(file_path, new_file_path)
                print ("Back up file : " + name + " - Success")

                logger.info(file_path + " - is stored to myArchive\n")

            else:
                existed_files_counter += 1

    logger.info(str(new_files_counter) + " - NEW FILES are stored to myArchive\n")
    logger.info(str(existed_files_counter) + " FILES didnt need to be backed up because they're already in the archive\n")


    print (str(new_files_counter) + " NEW FILES have been backed up successfully")
    print (str(existed_files_counter) + " FILES didnt need to be backed up because they're already in the archive")


    if os.path.isfile(indexPath):
        json.dump(index, open(indexPath, 'w'))