import hashFinder, os, json, shutil
from hashFinder import createFileSignature
from log import log


def test(objectsDir,indexPath):
    path_errors = [];
    hash_errors = [];
    correct = 0;
    index = {};
    logger = log();
    logger.info("Test session stated\n");

    try:
        with open(indexPath) as file:
            index = json.load(file);
    except (OSError, IOError):
        print ("Error loading index file. Index could be missing or not created yet");
        logger.error("Error loading index file. Index could be missing or not created yet\n");
        return;

    for key in index.keys():
        file_path_check = True;
        file_hash_check = True;
        # Check if file with object is tied to object
        file_path = os.path.join(objectsDir,index[key]);
        if not os.path.exists(file_path):
            path_errors.append((key, index[key], file_path));
            file_path_check = False;

        #Check hash of contents and compare to filename
        signature = createFileSignature(os.path.join(objectsDir,index[key]));
        if (signature == None):
            hash_errors.append((key,index[key]));
            file_hash_check = False;
            
        elif (index[key] != signature[2]):
            hash_errors.append((key,index[key], signature[2]));
            file_hash_check = False;

        if (file_hash_check and file_path_check):
            correct+=1;


    #Print results
    print ("There are " + str(correct) + " correct entries");
    logger.info(str(correct) + " correct entries running test\n");
    
    if (len(path_errors) == 0):
        print ("There are no false paths");
        logger.info("No false paths running test\n");
    else:
        print ("\n" + str(len(path_errors)) + " path errors:")
        for item in path_errors: #Listed as (path, hash/object, file_path)
            print ("Object " + item[1] + " (as " + item[0] + ") does not exist at " + item[2] + "\n");
            logger.warning("Object " + item[1] + " (as " + item[0] + ") does not exist at " + item[2] + "\n");

    if (len(hash_errors) == 0):
        print ("There are no hashing errors");
    else:
        print ("\n" + str(len(hash_errors)) + " hashing errors:")
        for item in hash_errors: #Listed as (path, hash/object, expected hash)
            if (len(item) == 2):
                print ("Could not check the hash of " + item[0] + "\n");
            else:  
                print (item[0] + " content hash does not match (expected " + item[1] + " but got " + item[2] + "\n");
                logger.warning(item[0] + " content hash does not match (expected " + item[1] + " but got " + item[2] + "\n");
